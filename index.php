<?php
require_once 'animal.php';
require_once 'Ape.php';
require_once 'Frog.php';

$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo "<br>";
echo $sheep->legs; // 2
echo "<br>";
echo $sheep->cold_blooded; // false

$sungokong = new Ape("kera sakti");
echo "<br><br>";
echo $sungokong->name; // "kera sakti"
echo "<br>";
echo $sungokong->legs; // 2
echo "<br>";
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
echo "<br><br>";
echo $kodok->name; // "buduk"
echo "<br>";
echo $kodok->legs; // 4
echo "<br>";
$kodok->jump(); // "hop hop"

?>